import React from "react";
import TodoList from "../models/todolist";
import ItemView, { OnDeleteHandler, OnToggleHandler } from "./itemview";

export interface TodoListViewProperties {
    list: TodoList;
    onToggle: OnToggleHandler;
    onDelete: OnDeleteHandler;
}

export class TodoListView extends React.Component<TodoListViewProperties> {
    render(): React.ReactNode {
        return (
            <div className="todolist">
                { this.props.list.items().map((item) =>
                    <ItemView
                        key={item.title}
                        item={item}
                        onToggle={(item) => this.props.onToggle(item)}
                        onDelete={(item) => this.props.onDelete(item)}
                        />
                ) }
            
            </div>
        );
    }
}

export default TodoListView;