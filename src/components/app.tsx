import React from "react";
import Item from "../models/item";
import TodoList from "../models/todolist";
import TodoForm from "./todoform";
import TodoListView from "./todolistview";

export interface AppProperties{}

export interface AppState{
    todos: TodoList
}

export class App extends React.Component<AppProperties, AppState> {
    constructor(props: AppProperties) {
        super(props);
        this.state = {
            todos: new TodoList()
        }
    }

    render(): React.ReactNode {
        return (
            <div className="root">
                <h1>Todos</h1>
                <TodoForm onAdd={(item: Item) => this.add(item)} />
                <TodoListView
                    list={this.state.todos}
                    onToggle={(item) => this.toggle(item.title) }
                    onDelete={(item) => this.delete(item)}
                    />
            </div>
        );
    }

    private toggle(title: string) {
        const todos = this.state.todos;

        todos.toggle(title);

        this.setState({
            todos: todos,
        });
    }

    private add(item: Item) {
        const todos = this.state.todos;

        todos.add(item);

        this.setState({
            todos: todos,
        });
    }

    private delete(item: Item) {
        const todos = this.state.todos;

        todos.remove(item.title);

        this.setState({
            todos: todos,
        });
    }
}

export default App;