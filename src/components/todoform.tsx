import React from "react";
import Item from "../models/item";

export type OnAddItem = (item: Item) => void;

export interface TodoFormProperties {
    onAdd: OnAddItem;
}

export class TodoFormState {
    title?: string;
    description?: string;
}

export class TodoForm extends React.Component<TodoFormProperties, TodoFormState> {
    constructor(props: TodoFormProperties) {
        super(props);
        this.state = {}
    }

    render(): React.ReactNode {
        return (
            <div>
                <input
                    type="text"
                    name="title"
                    placeholder="Title..."
                    value={this.state.title ?? ""}
                    onChange={ (event) => this.updateTitle(event.target.value) }
                    />
                <input
                    type="text"
                    name="description"
                    placeholder="Description..."
                    value={this.state.description ?? ""}
                    onChange={ (event) =>
                        this.updateDescription(event.target.value) }
                    />
                <button onClick={ () => this.add() }>Save</button>
            </div>
        );
    }

    private updateTitle(title: string) {
        this.setState(Object.assign(this.state, {title: title}));
    }

    private updateDescription(description: string) {
        this.setState(Object.assign(this.state, {description: description}));
    }

    add() {
        if (this.state.title == null) {
            return;
        }

        const item = new Item(this.state.title, false, this.state.description);

        this.state = {};

        this.props.onAdd(item);
    }
}

export default TodoForm;