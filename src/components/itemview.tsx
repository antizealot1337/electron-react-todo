import React from "react";
import Item from "../models/item";

export type OnToggleHandler = (item: Item) => void;
export type OnDeleteHandler = (item: Item) => void;

export interface ItemViewProperties {
    item: Item;
    onToggle: OnToggleHandler;
    onDelete: OnDeleteHandler;
}

export class ItemView extends React.Component<ItemViewProperties> {
    render(): React.ReactNode {
        const item = this.props.item;

        return (
            <div className="itemview">
                <h3>{ item.title }</h3>
                <div>{ item.description }</div>
                <div>{ item.done ? 'Completed' : 'Pending' }</div>
                <button onClick={ () => this.props.onToggle(item) }>Toggle</button>
                <button onClick={ () => this.props.onDelete(item) }>Delete</button>
            </div>
        );
    }
}

export default ItemView;