export class Item {
    title: string
    description?: string
    done: boolean

    constructor(title: string, done: boolean, description?: string) {
        this.title = title;
        this.description = description;
        this.done = done;
    }

    toggle() {
        this.done = !this.done
    }
}

export default Item;