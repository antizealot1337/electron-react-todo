import Item from "./item";

export class TodoList {
    _items: Item[] = [];

    add(item: Item) {
        if (this.find(item.title) != null) {
            return;
        }
        this._items.push(item);
    }

    remove(title: string) {
        this._items = this._items.filter((item) => item.title != title);
    }

    toggle(title: string) {
        this.find(title)?.toggle()
    }

    find(title: string): (Item|null) {
        return this._items.find((item: Item) => item.title == title) ?? null;
    }

    items(): readonly Item[] {
        return this._items;
    }
}

export default TodoList