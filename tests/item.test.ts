import Item from '../src/models/item';

import {expect} from 'chai';

describe('item', () => {
    it('can toggle', () => {
        let todo = new Item('title', false);

        expect(todo.done).to.equal(false);

        todo.toggle();

        expect(todo.done).to.equal(true);
    })
})