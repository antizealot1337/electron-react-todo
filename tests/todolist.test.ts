import Item from '../src/models/item';
import TodoList from '../src/models/todolist';

import {expect} from 'chai';

describe('todolist', () => {
    it('can add an Item', () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);

        expect(todos._items.length).to.equal(1);
    })
    it("will ignore an Item with the same title", () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);
        todos.add(todo);

        expect(todos._items.length).to.equal(1);
    })
    it('can remove an Item', () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);
        todos.remove(todo.title);

        expect(todos._items.length).to.equal(0);
    })
    it('can find an item', () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);

        expect(todos.find('nothing')).to.equal(null);
        expect(todos.find(todo.title)).to.equal(todo);
    })
    it('can toggle an item', () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);

        todos.toggle('nothing');

        expect(todo.done).to.equal(false);

        todos.toggle(todo.title)

        expect(todo.done).to.equal(true);
    })
    it('can return the items array (as readonly)', () => {
        let todo = new Item('title', false, 'description');
        let todos = new TodoList();

        todos.add(todo);

        expect(todos.items()).to.equal(todos._items);
    })
})